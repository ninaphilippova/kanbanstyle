﻿using System.Data.SqlClient;
using Dapper;
using KanbanStyle.Migrations;

namespace KanbanStyle.Tests.Integration.Helpers {
    public class TestDatabase {
        private readonly string _masterConnectionString;

        public TestDatabase(string connectionString) {
            ConnectionString = connectionString;
            Name = new SqlConnectionStringBuilder(connectionString).InitialCatalog;
            _masterConnectionString = new SqlConnectionStringBuilder(connectionString) {
                InitialCatalog = "master"
            }.ConnectionString;
        }

        public string ConnectionString { get; }
        public string Name { get; }

        public void Create() {
            using (var connection = new SqlConnection(_masterConnectionString)) {
                connection.Execute($"create database [{Name}]");
            }
            Migrator.MigrateToLatest(ConnectionString);
        }

        public void Reset() {
            using (var connection = new SqlConnection(ConnectionString)) {
                connection.Execute($@"
                    EXEC sp_MSForEachTable 'DISABLE TRIGGER ALL ON ?'
                    EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
                    EXEC sp_MSForEachTable 'DELETE FROM ?'
                    EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'
                    EXEC sp_MSForEachTable 'ENABLE TRIGGER ALL ON ?'
                ");
            }
        }
        

        public void DropIfExists() {
            using (var connection = new SqlConnection(_masterConnectionString)) {
                connection.Execute($@"
                    if not exists (select * from sys.databases where name='{Name}')
                        return
                    
                    alter database [{Name}] set single_user with rollback immediate
                    drop database [{Name}];
                ");
            }
        }
    }
}
