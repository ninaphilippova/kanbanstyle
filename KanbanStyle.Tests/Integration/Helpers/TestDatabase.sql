﻿SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;

USE [$(DatabaseName)];
PRINT N'Creating [dbo].[Cards]...'
CREATE TABLE [dbo].[Cards] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [StatusId]    TINYINT        NULL,
    [SortOrder]   INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


PRINT N'Creating [dbo].[Statuses]...';


CREATE TABLE [dbo].[Statuses] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [SortOrder] TINYINT       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


PRINT N'Update complete.';
