﻿using NUnit.Framework;
using System.Data.SqlClient;
using Dapper;
using KanbanStyle.Model;
using KanbanStyle.Controllers;
using KanbanStyle.Security;
using System.Net.Http;

namespace KanbanStyle.Tests.Integration {
    using System.Net;
    using static HttpMethod;

    public class LoginTests : WebApiTestsBase {
        [TestCase("username", "password", true)]
        [TestCase("username_other", "password", false)]
        [TestCase("username", "password_other", false)]
        [TestCase("username", "", false)]
        public void Post_CheckIfUserCanLogin(string username, string password, bool expectedSuccess) {
            var users = new[] { CreateUser("username", "password") };

            var login = new UserController.Login {
                Username = username,
                Password = password
            };
            var response = Send(Post, "/api/login/", login, ensureSuccess: false);
            Assert.AreEqual(
                expectedSuccess ? HttpStatusCode.OK : HttpStatusCode.Unauthorized,
                response.StatusCode
            );
            Assert.AreEqual(expectedSuccess, response.Content.ReadAsAsync<string>().Result != null);
        }

        private User CreateUser (string username, string password) {
            var user = new User {
                Username = username,
                Password = ""
            };
            using (var connection = new SqlConnection(Database.ConnectionString)) {
                user.Id = connection.ExecuteScalar<int>(@"
                    insert dbo.Users (FirstName, Username, Password) values (@UserName, @UserName, @Password)
                    select scope_identity()
                ", user);

                user.Password = new HashGenerator().Generate(password, user.Id);
                connection.Execute(@"
                    update dbo.Users set Password = @Password where Id = @Id
                ", user);
            }
            return user;
        }
    }
}
