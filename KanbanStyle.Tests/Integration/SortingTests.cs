﻿using System;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using NUnit.Framework;
using KanbanStyle.Model;
using System.Net.Http;

namespace KanbanStyle.Tests.Integration {
    using static HttpMethod;

    public class SortingTests : WebApiTestsBase {
        [TestCase("C3", "S1", 1, new[] {
            "C3 S1:1", "C1 S1:2", "C2 S1:3", 
            "C4 S2:1", "C5 S2:2", "C6 S2:3",
            "C7 S4:1"
        })]
        [TestCase("C1", "S1", 3, new[] {
            "C2 S1:1", "C3 S1:2", "C1 S1:3",
            "C4 S2:1", "C5 S2:2", "C6 S2:3",
            "C7 S4:1"
        })]
        [TestCase("C2", "S1", 2, new[] {
            "C1 S1:1", "C2 S1:2", "C3 S1:3",
            "C4 S2:1", "C5 S2:2", "C6 S2:3",
            "C7 S4:1"
        })]
        [TestCase("C1", "S2", 2, new[] {
            "C2 S1:1", "C3 S1:2",
            "C4 S2:1", "C1 S2:2", "C5 S2:3", "C6 S2:4",
            "C7 S4:1"
        })]
        [TestCase("C1", "S3", 1, new[] {
            "C2 S1:1", "C3 S1:2",
            "C4 S2:1", "C5 S2:2", "C6 S2:3",
            "C1 S3:1",
            "C7 S4:1"
        })]
        [TestCase("C7", "S1", 4, new[] {
            "C1 S1:1", "C2 S1:2", "C3 S1:3", "C7 S1:4",
            "C4 S2:1", "C5 S2:2", "C6 S2:3"
        })]
        public void Put_UpdatesOtherCards_WhenSortOrderIsChanged(
            string cardTitle, string newStatusName, int newSortOrder, string[] expected
        ) {
            var statuses = new[] { CreateStatus("S1", 1), CreateStatus("S2", 2), CreateStatus("S3", 3), CreateStatus("S4", 4) };
            var statusesById = statuses.ToDictionary(s => s.Id);
            var cards = new[] {
                CreateCard("C1", statuses[0], 1),
                CreateCard("C2", statuses[0], 2),
                CreateCard("C3", statuses[0], 3),
                CreateCard("C4", statuses[1], 1),
                CreateCard("C5", statuses[1], 2),
                CreateCard("C6", statuses[1], 3),
                CreateCard("C7", statuses[3], 1),
            };
            
            var card = cards.First(c => c.Title == cardTitle);
            card.StatusId = statuses.First(s => s.Name == newStatusName).Id;
            card.SortOrder = newSortOrder;
            Send(Put, "/api/cards/" + card.Id, card);

            var reloaded = Send(Get, "/api/cards").Content.ReadAsAsync<Card[]>().Result;
            CollectionAssert.AreEquivalent(
                expected,
                reloaded.Select(c => $"{c.Title} {statusesById[c.StatusId].Name}:{c.SortOrder}")
            );
        }

        [TestCase("C3", new[] {
            "C1 S1:1", "C2 S1:2",
            "C4 S2:1",
        })]
        [TestCase("C1", new[] {
            "C2 S1:1", "C3 S1:2",
            "C4 S2:1", 
        })]
        [TestCase("C2", new[] {
            "C1 S1:1", "C3 S1:2",
            "C4 S2:1", 
        })]
        [TestCase("C4", new[] {
            "C1 S1:1", "C2 S1:2", "C3 S1:3",
        })]
        public void Delete_UpdatesOtherCards(
            string cardTitle, string[] expected
        ) {
            var statuses = new[] { CreateStatus("S1", 1), CreateStatus("S2", 2) };
            var statusesById = statuses.ToDictionary(s => s.Id);
            var cards = new[] {
                CreateCard("C1", statuses[0], 1),
                CreateCard("C2", statuses[0], 2),
                CreateCard("C3", statuses[0], 3),
                CreateCard("C4", statuses[1], 1)
            };

            var card = cards.First(c => c.Title == cardTitle);
            Send(Delete, "/api/cards/" + card.Id);

            var reloaded = Send(Get, "/api/cards").Content.ReadAsAsync<Card[]>().Result;
            CollectionAssert.AreEquivalent(
                expected,
                reloaded.Select(c => $"{c.Title} {statusesById[c.StatusId].Name}:{c.SortOrder}")
            );
        }

        private Status CreateStatus(string name = null, int sortOrder = 1) {
            var status = new Status {
                Name = name ?? "Test",
                SortOrder = sortOrder
            };
            using (var connection = new SqlConnection(Database.ConnectionString)) {
                status.Id = connection.ExecuteScalar<int>(@"
                    insert dbo.Statuses (Name, SortOrder) values (@Name, @SortOrder)
                    select scope_identity()
                ", status);
            }
            return status;
        }

        private Card CreateCard(string title, Status status, int sortOrder) {
            var card = new Card {
                StatusId = status.Id,
                Title = title,
                SortOrder = sortOrder
            };
            return Send(Post, "/api/cards", card).Content.ReadAsAsync<Card>().Result;
        }
    }
}
