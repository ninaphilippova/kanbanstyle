﻿using NUnit.Framework;
using System.Data.SqlClient;
using Dapper;
using KanbanStyle.Model;
using KanbanStyle.Controllers;
using KanbanStyle.Security;
using System.Net.Http;


namespace KanbanStyle.Tests.Integration {
    using System.Net;
    using System.Net.Http.Headers;
    using static HttpMethod;

    public class TokenTests : WebApiTestsBase {
        [TestCase("123")]
        [TestCase("Bearer")]
        [TestCase("Bearer ")]
        [TestCase(@"Bearer K+NH7URE/7ZRGDv8WwOKvguGqGyipMRKpMilI4pqtY+YiQPnzrP9F2FQTfsbqGIcEzbhRe/FAhzfyIHPlpDynbzzWpyoMPJe8v5TRDxqW5Q=")]
        public void CheckCallFails(string value) {
            var response = WebApi.HttpClient.SendAsync(new HttpRequestMessage(Get, "/api/cards/") {
                Headers = {
                    { "Authorization", value }
                }
            }).Result;
            Assert.AreEqual(
                            HttpStatusCode.Unauthorized,
                            response.StatusCode
                        );
        }

        [TestCase("GET", "/api/cards/")]
        [TestCase("GET", "/api/statuses/")]
        [TestCase("POST", "api/cards")]
        [TestCase("DELETE", "api/cards/18")]
        public void CheckCallFailsIfTokenIsNull(string method, string url) {
            var httpMethod = new HttpMethod(method);
            var response = WebApi.HttpClient.SendAsync(new HttpRequestMessage(httpMethod, url)).Result;
            Assert.AreEqual(
                            HttpStatusCode.Unauthorized,
                            response.StatusCode
                        );
        }

    }
}

