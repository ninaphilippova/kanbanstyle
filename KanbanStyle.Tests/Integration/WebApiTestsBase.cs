﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using KanbanStyle.Tests.Integration.Helpers;
using System.Configuration;
using Microsoft.Owin.Testing;
using Owin;
using System.Web.Http;
using KanbanStyle.Model;
using System.Net.Http;
using KanbanStyle.Security;
using System.Net.Http.Headers;

namespace KanbanStyle.Tests.Integration {
    public abstract class WebApiTestsBase {
        [OneTimeSetUp]
        public void BeforeAllTests() {
            Database.DropIfExists();
            Database.Create();
        }

        [SetUp]
        public void BeforeEachTest() {
            Database.Reset();
            WebApi = TestServer.Create(app => {
                var configuration = new HttpConfiguration();
                WebApiConfig.Register(configuration);
                configuration.EnsureInitialized();
                app.UseWebApi(configuration);

                WebApiConfiguration = configuration;
            });
        }

        [TearDown]
        public void AfterEachTest() {
            WebApi.Dispose();
        }

        protected HttpConfiguration WebApiConfiguration { get; private set; }
        protected TestServer WebApi { get; private set; }
        protected TestDatabase Database { get; } = new TestDatabase(ConnectionStringConfiguration.ConnectionString);

        protected HttpResponseMessage Send(HttpMethod method, string url, object data = null, bool ensureSuccess = true, AuthenticationHeaderValue auth = null) {
            var tokenGenerator = (TokenGenerator)WebApiConfiguration.DependencyResolver.GetService(typeof(TokenGenerator));
            var token = tokenGenerator.Generate(1);
            var content = data != null ? new ObjectContent(data.GetType(), data, WebApiConfiguration.Formatters.JsonFormatter) : null;
            var response = WebApi.HttpClient.SendAsync(new HttpRequestMessage(method, url) {
                Headers = {
                    Authorization = auth ?? new AuthenticationHeaderValue("Bearer", token)
                },
                Content = content
            }).Result;
            if (ensureSuccess) {
                response.EnsureSuccessStatusCode();
            }
            return response;
        }
    }
}
