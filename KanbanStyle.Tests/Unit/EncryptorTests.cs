﻿using KanbanStyle.Security;
using NUnit.Framework;

namespace KanbanStyle.Tests.Unit {
    public class EncryptorTests {
        [TestCase("") ]
        [TestCase("01")]
        [TestCase("1")]
        [TestCase("Ww!@#$%_.")]
        public void CheckStringsCanBeDecrypted (string password) {
            var encryptor = new Encryptor();
            var encrypted = encryptor.Encrypt(password);
            Assert.AreEqual(password, encryptor.Decrypt(encrypted));
        }
    }
}
