﻿using KanbanStyle.Tests.Integration;
using KanbanStyle.Security;
using NUnit.Framework;
using System.Net.Http;

namespace KanbanStyle.Tests {
    public class PasswordTests {
        [TestCase(1, "1", "4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8")]
        [TestCase(2, "4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8", "eb3be34c433f76755a049fc9ff8106c79191f9a650ea5b458a4284efab271985")]
        public void ChecksIfPasswordMatches(int userId, string password, string expectedPassword) {
            var hashedPassword = new HashGenerator().Generate(password, userId);
            Assert.AreEqual(
                expectedPassword,
                hashedPassword
            );
        }
    }
}
