﻿using KanbanStyle.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;
using KanbanStyle.Repositories;
using KanbanStyle.Security;

namespace KanbanStyle.Controllers {
    [RoutePrefix("api/cards")]
    public class CardController : ApiController {
        private readonly CardRepository _repository;
        
        public CardController(CardRepository repository) {
            _repository = repository;
        }

        [Route("")]
        public IReadOnlyList<Card> GetAll() {
            return _repository.GetAll();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody] Card card) { 
            _repository.Save(card);
            return Created(new Uri("todo:"), card);
        }
        
        [HttpPut]
        [Route("{id}")]
        public void Put(Card card) {
            _repository.Update(card);
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id) {
            _repository.Delete(id);
        }
    }
}
