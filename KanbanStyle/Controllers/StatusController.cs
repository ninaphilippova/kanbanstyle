﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KanbanStyle.Repositories;
using KanbanStyle.Model;

namespace KanbanStyle.Controllers {
    [RoutePrefix("api/statuses")]
    public class StatusController : ApiController {
        private readonly StatusRepository _repository = new StatusRepository(ConnectionStringConfiguration.ConnectionString);

        [Route("")]
        public IReadOnlyList<Status> GetAll() {
            return _repository.GetAll();
        }
    }
}
