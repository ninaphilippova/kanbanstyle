﻿using KanbanStyle.Model;
using System;
using System.Web.Http;
using KanbanStyle.Repositories;
using KanbanStyle.Security;

namespace KanbanStyle.Controllers {
    [RoutePrefix("api/login")]
    public class UserController : ApiController {
        private readonly UserRepository _repository;
        private readonly TokenGenerator _tokenGenerator;
        private readonly HashGenerator _hashGenerator;

        public UserController(UserRepository repository, TokenGenerator tokenGenerator, HashGenerator hashGenerator) {
            _repository = repository;
            _tokenGenerator = tokenGenerator;
            _hashGenerator = hashGenerator;
        }

        [AllowAnonymous]
        [Route("")]
        public IHttpActionResult Post([FromBody] Login login) {
            var user = _repository.FindUser(login.Username);
            if (user == null) {
                return Unauthorized();
            }
            var hashedPassword = _hashGenerator.Generate(login.Password, user.Id);
            if (user.Password != hashedPassword) {
                return Unauthorized();
            }
            
            var tokenString = _tokenGenerator.Generate(user.Id);
            return Ok(tokenString);
        }

        public class Login {
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}