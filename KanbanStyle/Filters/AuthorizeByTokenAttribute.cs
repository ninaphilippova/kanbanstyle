﻿using KanbanStyle.Security;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace KanbanStyle.Filters {
    public class AuthorizeByTokenAttribute : AuthorizeAttribute {
        private readonly TokenGenerator _tokenGenerator;

        public AuthorizeByTokenAttribute(TokenGenerator tokenGenerator) {
            _tokenGenerator = tokenGenerator;
        }

        protected override bool IsAuthorized(HttpActionContext context) {
            var token = context.Request.Headers.Authorization?.Parameter;
            return _tokenGenerator.Validate(token);
        }
    }
}