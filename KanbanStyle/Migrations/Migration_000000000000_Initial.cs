﻿using System;
using FluentMigrator;


namespace KanbanStyle.Migrations {
    [Migration(000000000000)]
    public class Migration_000000000000_Initial : ForwardOnlyMigration {
        public override void Up() {
            Create.Table("Statuses")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Name").AsCustom("NVARCHAR(MAX)").Nullable()
                .WithColumn("SortOrder").AsInt32();

            Create.Table("Cards")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Title").AsCustom("NVARCHAR(MAX)").Nullable()
                .WithColumn("Description").AsCustom("NVARCHAR(MAX)").Nullable()
                .WithColumn("StatusId").AsInt32().ForeignKey("Statuses", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("SortOrder").AsInt32();

        }
    }
}