﻿using System;
using FluentMigrator;

namespace KanbanStyle.Migrations {
    [Migration(000000000001)]
    public class Migration_201611020956_Users : ForwardOnlyMigration {
        public override void Up() {
            Create.Table("Users")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("FirstName").AsCustom("NVARCHAR(30)")
                .WithColumn("LastName").AsCustom("NVARCHAR(30)").Nullable()
                .WithColumn("Username").AsCustom("NVARCHAR(30)").Unique()
                .WithColumn("Password").AsCustom("NVARCHAR(64)");

        }
    }
}