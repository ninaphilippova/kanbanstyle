﻿using System.Reflection;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;

namespace KanbanStyle.Migrations {
    public static class Migrator {
        public static void MigrateToLatest(string connectionString) {
            var announcer = new NullAnnouncer(); // new TextWriterAnnouncer(s => Debug.WriteLine(s));
            var migrationContext = new RunnerContext(announcer);

            var options = new ProcessorOptions();
            var factory = new SqlServer2008ProcessorFactory();

            using (var processor = factory.Create(connectionString, announcer, options)) {
                var assembly = Assembly.GetExecutingAssembly();
                var runner = new MigrationRunner(assembly, migrationContext, processor);
                runner.MigrateUp(true);
            }
        }
    }
}