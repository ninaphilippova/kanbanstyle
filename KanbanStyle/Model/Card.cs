﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KanbanStyle.Model {
    public class Card : Entity {
        public string Title { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
    }
}