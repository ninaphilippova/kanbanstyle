﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace KanbanStyle.Model {
    public static class ConnectionStringConfiguration {
        public static string ConnectionString { get; } = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
    }
}