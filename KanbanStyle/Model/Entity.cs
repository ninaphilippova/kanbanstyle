﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KanbanStyle.Model {
    public abstract class Entity {
        public int Id { get; set; }
        public int SortOrder { get; set; }
    }
}