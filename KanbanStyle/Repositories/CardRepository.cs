﻿using KanbanStyle.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace KanbanStyle.Repositories {
    public class CardRepository {
        private readonly string _connectionString;

        public CardRepository(string connectionString) {
            _connectionString = connectionString;
        }

        /*
        public IList<Status> GetStatuses() {
            using (var connection = new SqlConnection(_connectionString)) {
                return connection.Query<Status>(@"
                    select * from dbo.Statuses
                    ").ToList();
            }
        }*/

        public IReadOnlyList<Card> GetAll() {
            using (var connection = new SqlConnection(_connectionString)) {
                return connection.Query<Card>(@"
                    select * from dbo.Cards
                    order by SortOrder asc
                    ").ToList();
            }
        }

        public void Save(Card card) {
            using (var connection = new SqlConnection(_connectionString)) {
                card.Id = connection.ExecuteScalar<int>(@"
                    update dbo.Cards
                    set SortOrder += 1
                    where StatusId = @StatusId
                      and SortOrder >= @SortOrder

                    insert into dbo.Cards (Title, Description, StatusId, SortOrder) values (@Title, @Description, @StatusId, @SortOrder)
                    select scope_identity()
                ", card);
            }
            Console.WriteLine(card.Id);
        }

        public void Update(Card card) {
            using (var connection = new SqlConnection(_connectionString)) {
                connection.Execute(@"
                    declare @deleted table (
                        StatusId int,
	                    SortOrder int
                    )
                    update dbo.Cards 
                    set Title = @Title, 
                        Description = @Description,
                        StatusId = @StatusId,
                        SortOrder = @SortOrder
                    output deleted.StatusId, deleted.SortOrder into @deleted
                    where Id = @Id

                    update [card]
                    set [card].SortOrder = [card].SortOrder - 1
                    from dbo.Cards as [card]
                    where [card].StatusId = (select d.StatusId from @deleted as d)
                    and [card].StatusId <> (select c.StatusId from dbo.Cards as c where Id = @id)
                    and [card].SortOrder > (select d.SortOrder from @deleted as d)
                    and [card].Id <> @Id

                    update [card]
                    set [card].SortOrder = [card].SortOrder - 1
                    from dbo.Cards as [card] 
                    inner join @deleted as [deleted] 
                    on [card].StatusId = [deleted].StatusId
                    and [card].StatusId = (select c.StatusId from dbo.Cards as c where Id = @id)
		            and [card].SortOrder > [deleted].SortOrder
                    and [card].SortOrder <= (select c.SortOrder from dbo.Cards as c where Id = @id)
                    and [card].Id <> @Id
                    
                    update [card]
                    set [card].SortOrder = [card].SortOrder + 1
                    from dbo.Cards as [card]
                    inner join @deleted as [deleted] 
                    on [card].StatusId = [deleted].StatusId
                    and [card].StatusId = (select c.StatusId from dbo.Cards as c where Id = @id)
                    and [card].SortOrder >= (select c.SortOrder from dbo.Cards as c where Id = @id)
                    and [card].SortOrder < [deleted].SortOrder
                    and [card].Id <> @Id
               
                    update [card]
                    set [card].SortOrder = [card].SortOrder + 1
                    from dbo.Cards as [card]
                    where [card].StatusId <> (select d.StatusId from @deleted as d)
                    and [card].StatusId = (select c.StatusId from dbo.Cards as c where Id = @id)
                    and [card].SortOrder >= (select c.SortOrder from dbo.Cards as c where Id = @id)
                    and [card].Id <> @Id
                  

                    
                ", card);
            }
        }

        public void Delete(int id) {
            using (var connection = new SqlConnection(_connectionString)) {
                connection.Execute(@"
                    declare @deleted table (
	                    StatusId int,
	                    SortOrder int
                    )
                    delete from dbo.Cards
                    output deleted.StatusId, deleted.SortOrder into @deleted
                    where Id = @id
					
                    update [card]
                    set SortOrder -= 1
                    from
	                    dbo.Cards as [card]
	                    inner join @deleted AS [deleted] on
		                    [card].StatusId = [deleted].StatusId
		                    and
		                    [card].SortOrder > [deleted].SortOrder
                ", new { id });
            }
        }

    }
}