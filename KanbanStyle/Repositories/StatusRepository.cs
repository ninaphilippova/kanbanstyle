﻿using KanbanStyle.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace KanbanStyle.Repositories {
    public class StatusRepository {
        private readonly string _connectionString;
        
        public StatusRepository(string connectionString) {
            _connectionString = connectionString;
        }

        public IReadOnlyList<Status> GetAll() {
            using (var connection = new SqlConnection(_connectionString)) {
                return connection.Query<Status>(@"
                    select * from dbo.Statuses
                    order by SortOrder asc
                    ").ToList();
            }
        }
    }
}