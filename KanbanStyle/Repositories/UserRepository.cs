﻿using System;
using System.Collections.Generic;
using System.Linq;
using KanbanStyle.Model;
using System.Data.SqlClient;
using Dapper;

namespace KanbanStyle.Repositories {
    public class UserRepository {
       private readonly string _connectionString;

       public UserRepository(string connectionString) {
           _connectionString = connectionString;
       }

       public User FindUser(string username) {
         using (var connection = new SqlConnection(_connectionString)) {
             return connection.Query<User>(@"
                select * from dbo.Users where Username = @username
            ", new { username }).SingleOrDefault();
        }
    }
}
}