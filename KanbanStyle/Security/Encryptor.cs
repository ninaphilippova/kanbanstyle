﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KanbanStyle.Model;
using System.Web.Security;
using System.Text;

namespace KanbanStyle.Security {
    public class Encryptor {
        public string Encrypt(string value) {
            return Convert.ToBase64String(MachineKey.Protect(Encoding.UTF8.GetBytes(value)));
        }

        public string Decrypt(string value) {
            return Encoding.UTF8.GetString(MachineKey.Unprotect(Convert.FromBase64String(value)));
        }
    }
}