﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KanbanStyle.Security {
    public class HashGenerator {
        /*public string GenerateSalt(int salt) {
            var saltString = "";
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[salt];
            rng.GetBytes(buffer);
            foreach (byte x in buffer) {
                saltString += String.Format("{0:x2}", x);
            }
            return saltString;
        }*/

        public string Generate(string password, int userId) { 
            var input = Encoding.UTF8.GetBytes(password + userId.ToString());
            using (var sha = SHA256.Create()) {
                var bytes = sha.ComputeHash(input);
                return new SoapHexBinary(bytes).ToString().ToLowerInvariant();
            }
        }
    }
}