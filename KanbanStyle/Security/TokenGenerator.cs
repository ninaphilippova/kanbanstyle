﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace KanbanStyle.Security {
    public class TokenGenerator {
        private Encryptor _encryptor;

        public TokenGenerator(Encryptor encryptor) {
            _encryptor = encryptor;
        }

        public bool Validate(string token) {
            if (string.IsNullOrEmpty(token)) {
                return false;
            }
            try {
                var decryptedString = _encryptor.Decrypt(token);
                var parts = decryptedString.Split('_');
                if (parts.Length < 2) {
                    return false;
                }
                var tokenTimeString = parts[1];
                var oldestTokenTime = DateTime.Now - TimeSpan.FromDays(2);
                var tokenTime = new DateTime(long.Parse(tokenTimeString));
                return tokenTime > oldestTokenTime;
            }
            catch (Exception ex) when (ex is FormatException || ex is CryptographicException) {
                return false;
            }
        }

        public string Generate(int userId) {
            var value = userId.ToString() + "_" + DateTime.Now.Ticks;
            return _encryptor.Encrypt(value);
        }
    }
}