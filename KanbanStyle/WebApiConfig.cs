﻿using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Autofac;
using System.Reflection;
using Autofac.Integration.WebApi;
using KanbanStyle.Model;
using KanbanStyle.Filters;
using System.Web.Http.Filters;

namespace KanbanStyle {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            RegisterAutofac(config);

            config.Filters.Add((IAuthorizationFilter)config.DependencyResolver.GetService(typeof(AuthorizeByTokenAttribute)));
        }

        private static void RegisterAutofac(HttpConfiguration config) {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                   .AsSelf()
                   .WithParameter(new NamedParameter("connectionString", ConnectionStringConfiguration.ConnectionString));

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);
            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}