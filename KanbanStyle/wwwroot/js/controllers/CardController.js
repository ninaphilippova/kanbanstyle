﻿angular.module('app').controller('CardController', ['$scope', '$http', '$q', 'cardLoader', function ($scope, $http, $q, cardLoader) {
    'use strict';
    cardLoader.loadCards().then(function (statuses) {
        $scope.statuses = statuses;
    });

    $scope.add = function (title, description, status) {
        var statusid = status.id;
        var sortorder = 1;
        sortCards(status);
        var card = { title: title, description: description, statusid: statusid, sortorder: sortorder };
        $http.post('/api/cards', card).then(function (response) {
            status.cards.splice(0, 0, response.data);
        });
    };

    $scope.update = function (card) {
        $http.put('/api/cards/' + card.id, card);
    };

    $scope.move = function (event, index, card, newStatus) {
        var oldStatus = $scope.statuses.filter(function (s) { return s.id === card.statusId; })[0];
        var oldIndex = -1;
        for (var i = 0; i < oldStatus.cards.length; i++) {
            if (oldStatus.cards[i].id === card.id) {
                oldIndex = i;
                break;
            }
        }
        card.statusId = newStatus.id;
        oldStatus.cards.splice(oldIndex, 1);
        if (index > 0) {
            index -= 1;
        }
        if (oldStatus === newStatus && index > oldIndex) {
            index -= 1;
        }
        newStatus.cards.splice(index, 0, card);
        sortCards(newStatus);
        $http.put('/api/cards/' + card.id, card);
        return true;
    };

    $scope.remove = function (card, status) {
        $http.delete('/api/cards/' + card.id).then(function (response) {
            var index = status.cards.indexOf(card);
            status.cards.splice(index, 1);
            sortCards(status);
        });

    };

    $scope.export = function (a) {
        var json = JSON.stringify(a);
        var customCsv = '"Card Id","Title","Description","Status Id","Sort Order","Status Name"\r\n' + toCsv(a);
        var customXml = toXml(a);
        console.log(json);
        console.log(customCsv);
        console.log(customXml);
    };

    var sortCards = function (status) {
        for (var card of status.cards) {
            card.sortOrder = status.cards.indexOf(card) + 1;
        }
    };

    var toCsv = function (array) {
        var string = '';
        var statusName = '';
        for (var status of array) {
            statusName = status.name;
            for (var item of status.cards) {
                var quote = '"';
                var reg = new RegExp(quote, "g");
                string = string + '"' + item.id + '","' + item.title.replace(reg, '""') + '","' + item.description.replace('"', '""') + '","' + item.statusId + '","' + item.sortOrder + '","' + statusName + '"' + '\r\n';
            }
           
        }
        return string;
    };

    var toXml = function (array) {
        var string = '<?xml version="1.0" encoding="UTF-8"?><root>';
        for (var status of array) {
            string = string + '<status name="' + status.name + '" statusid="' + status.id + '" sortorder="' + status.sortOrder + '">';
            for (var item of status.cards) {
                string = string + '<card><id>' + item.id + '</id><title>' + item.title + '</title><sortorder>' + item.sortOrder + '</sortorder><description>' + item.description + '</description></card>';
            }
            string = string + '</status>';
        }
        string = string + '</root>';
        return string;
    };

}]);