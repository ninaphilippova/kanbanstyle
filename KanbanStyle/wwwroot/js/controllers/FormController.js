﻿angular.module('app').controller('FormController', ['$scope', '$http', '$window', 'tokenManager', function ($scope, $http, $window, tokenManager) {
    'use strict';
    $scope.failed = false;
    $scope.succeeded = tokenManager.ifSucceeded();
    $scope.username = localStorage.getItem('login.username');

    $scope.submit = function (login) {
        $http.post('/api/login', login).then(function (response) {
            $scope.failed = false;
            $scope.username = login.username;
            localStorage.setItem('login.username', login.username);
            $scope.succeeded = true;
            tokenManager.saveToken(response.data);
            
        }, function () {
            $scope.failed = true;
        });
    };

    $scope.logout = function () {
        $scope.succeeded = false;
        tokenManager.deleteToken();
        localStorage.removeItem('login.username');
        $window.location.reload();
    }
}]);