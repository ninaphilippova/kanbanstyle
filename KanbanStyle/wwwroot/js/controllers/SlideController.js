﻿angular.module('app').controller('SlideController', ['$scope', '$http', '$q', 'cardLoader', function ($scope, $http, $q, cardLoader) {
    'use strict';
    cardLoader.loadCardList().then(function (cards) {
        $scope.cards = cards;
        $scope.index = 0;
        $scope.card = cards[$scope.index];
        console.log($scope.card);
        $scope.listLength = $scope.cards.length;
    });
        
    $scope.next = function () {
        $scope.index += 1;
        if ($scope.index === $scope.listLength) {
            $scope.index = 0;
        } 
        $scope.card = $scope.cards[$scope.index];
        $scope.expand = false;
    }


}]);