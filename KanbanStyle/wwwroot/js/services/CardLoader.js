﻿angular.module('app').service('cardLoader', ['$http', '$q', 'tokenManager', function ($http, $q, tokenManager) {
    'use strict';
    var statuses = {};

    this.loadCards = function () {
        var requests = tokenManager.promise.then(function () {
            return $q.all([$http.get('/api/cards'), $http.get('/api/statuses')]);
        });

        return requests.then(function(responses) {
            var cards = responses[0].data;
            statuses = responses[1].data;
           
            for (var status of statuses) {
                var filteredCards = [];
                for (var card of cards) {
                    if (card.statusId === status.id) {
                        filteredCards.push(card);
                    }
                }
                status.cards = filteredCards;
            }
            return statuses;
        });
    }

    this.loadCardList = function () {
        var request = tokenManager.promise.then(function () {
            return $http.get('/api/cards');
        });

        return request.then(function (response) {
            var cards = response.data;
            return cards;
        });
    }

    this.clearCards = function () {
        $scope.statuses = null;
        return $scope.statuses;
    }
}])