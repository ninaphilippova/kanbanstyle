﻿angular.module('app').service('tokenManager', ['$http', '$q', function ($http, $q) {
    'use strict';
    var token = localStorage.getItem('login.token');
    var tokenDeferred = $q.defer();
    if (token) {
        applyToken();
    }

    this.promise = tokenDeferred.promise;

    this.saveToken = function (value) {
        token = value;
        localStorage.setItem('login.token', value);
        applyToken();
    }

    this.ifSucceeded = function () {
        return !!token;
    }

    this.deleteToken = function () {
        token = null;
        localStorage.removeItem('login.token');
        $http.defaults.headers.common.Authorization = null;
    }

    function applyToken() {
        $http.defaults.headers.common.Authorization = 'Bearer ' + token;
        tokenDeferred.resolve(token);
    }
}])